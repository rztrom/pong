//
//  GameViewController.m
//  ThePong
//
//  Created by Anders Zetterström on 2015-02-15.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "GameViewController.h"


@interface GameViewController ()
@property (weak, nonatomic) IBOutlet UIView *ball;
@property (weak, nonatomic) IBOutlet UIView *paddle;
@property (weak, nonatomic) IBOutlet UIView *computerPaddle;

@property (nonatomic) NSTimer *timer;
@property int xPos;
@property int yPos;
@property int playerPoints;
@property int computerPoints;
@property int counter;
@property (weak, nonatomic) IBOutlet UILabel *playerPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (weak, nonatomic) IBOutlet UILabel *computerPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *gameOutcomeLabel;
@property (weak, nonatomic) IBOutlet UIButton *exitGameButton;
@property (weak, nonatomic) IBOutlet UIButton *startGameButton;

@property (nonatomic) AVAudioPlayer *soundPlayer;

@property (nonatomic) AVAudioPlayer *looserSound;
@end

@implementation GameViewController

- (float) rightLimit {
    return self.view.superview.frame.size.width-(self.computerPaddle.frame.size.width/2);
}

-(float) leftLimit{
    return self.computerPaddle.frame.size.width/2;
}



-(void) fakeplayer{
    // paddle är till höger om bollen
    if(self.computerPaddle.center.x > self.ball.center.x){
        self.computerPaddle.center= CGPointMake(self.computerPaddle.center.x-1, self.computerPaddle.center.y);
    }
    // paddle är till vänster om bollen
    if(self.computerPaddle.center.x < self.ball.center.x){
        self.computerPaddle.center= CGPointMake(self.computerPaddle.center.x+1, self.computerPaddle.center.y);
    }
    
    // kan inte gå utanför på vänster sida
    if(self.computerPaddle.center.x < self.leftLimit){
        self.computerPaddle.center = CGPointMake(self.computerPaddle.frame.size.width/2, self.computerPaddle.center.y);
    }
    // kan inte gå utanför på höger sida
    if(self.computerPaddle.center.x > self.rightLimit){
        self.computerPaddle.center = CGPointMake(self.view.superview.frame.size.width-(self.computerPaddle.frame.size.width/2), self.computerPaddle.center.y);
    }
    
}

- (IBAction)startGameButton:(id)sender {
    [self gameStart];
    self.startGameButton.hidden=YES;
}

float clamp(float x, float min, float max) {
    if (x < min) {
        return min;
    } else if(x > max) {
        return max;
    } else {
        return x;
    }
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch =[[event allTouches] anyObject];
    CGPoint location= [touch locationInView:touch.view];
    self.paddle.center = CGPointMake(location.x, self.paddle.center.y);
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [self touchesBegan:touches withEvent:event];
}

-(void) paddleBounce{
    
    // om bollen träffar Paddle 1 (spelare)
    // vi vill ändra bollen till ett negativt y-värde från var den är
    if(CGRectIntersectsRect(self.ball.frame, self.paddle.frame)){
       //ljud
        [self.soundPlayer play];
        self.yPos= arc4random()%5;
        self.yPos= 0-self.yPos;
    }
    
    // om bollen träffar Paddle 2 (dator)
    // vi vill att bollen ska få ett positivt värde och studsa neråt
   if(CGRectIntersectsRect(self.ball.frame, self.computerPaddle.frame)){
      //ljud
       [self.soundPlayer play];
        self.yPos= arc4random()%5;
    }
    
}


-(void) startLayout{
    //placering av paddle
    self.paddle.center = CGPointMake(self.view.superview.frame.size.width/2, self.view.superview.frame.size.height- self.paddle.frame.size.height*2);
    //placering av boll
    self.ball.center= CGPointMake(self.view.superview.frame.size.width/2, self.view.superview.frame.size.height/2);
    // placering av datorns paddle
    self.computerPaddle.center = CGPointMake(self.view.superview.frame.size.width/2, self.computerPaddle.frame.size.height*2);
    //placering av startknapp
    self.startGameButton.center = CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2-self.ball.frame.size.height);
    //placering av text vid spelets slut
    self.gameOutcomeLabel.center= CGPointMake(self.view.superview.frame.size.width/2, self.view.superview.frame.size.height*0.35);
    //placering av exit-knapp
    self.exitGameButton.center = CGPointMake(self.view.superview.frame.size.width/2, self.view.superview.frame.size.height*0.75);
    //placering av spelarens poäng
    self.playerPointsLabel.center = CGPointMake(self.playerPointsLabel.frame.size.width, self.view.superview.frame.size.height-(self.playerPointsLabel.frame.size.height*5));
   //placering av datorns poäng
    self.computerPointsLabel.center = CGPointMake(self.computerPointsLabel.frame.size.width, self.computerPointsLabel.frame.size.height*5);
}

-(void) gameStart{
    
self.yPos= (arc4random()%11)-5+self.playerPoints; // slumpat nr mellan -5 & +5
self.xPos= (arc4random()%11)-5+self.playerPoints;

if(self.xPos == 0){
    self.xPos= 1; // vill inte att x ska vara 0
}

if(self.yPos == 0){
    self.yPos = 1;
}

self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(ballMotion) userInfo:nil repeats:YES];
}

-(void) ballMotion{
    
    [self fakeplayer];
    [self paddleBounce];
    self.ball.center= CGPointMake(self.ball.center.x+self.xPos, self.ball.center.y+self.yPos);
    
    
    // TODO - lägga i en if-sats med OR
    //TODO - byta värde till screen-size
    
    // bollen studsar vid kanten
    if (self.ball.center.x < self.ball.frame.size.width/2){
        self.xPos = 0- self.xPos;
        
    }
    if(self.ball.center.x > self.view.superview.frame.size.width-(self.ball.frame.size.width/2)){
        self.xPos = 0- self.xPos;
  
    }
    
    // kollar om spelare får poäng
    if(self.ball.center.y< (self.ball.frame.size.height*2)){
        //inkrementerar poängen
        self.playerPoints= self.playerPoints +1;
        // presenterar poäng
        self.playerPointsLabel.text=[NSString stringWithFormat:@"%i",self.playerPoints];
        // stänger av timer
        [self.timer invalidate];
        self.timer = nil;
        
        
        self.startGameButton.hidden= NO;
 
        

        // om spelet är slut (spelare kommer upp till 10 poäng)
        if(self.playerPoints == 10){
           self.startGameButton.hidden= YES;
            self.exitGameButton.hidden = NO;
            self.gameOutcomeLabel.hidden = NO;
            self.gameOutcomeLabel.text = [NSString stringWithFormat:@"You Win"];
        }
    }
    // kollar om dator får poäng
    if(self.ball.center.y> self.view.superview.frame.size.height- self.paddle.frame.size.height*2){
        //inkrementerar poängen
        self.computerPoints=self.computerPoints +1;
        //presenterar poäng
        self.computerPointsLabel.text = [NSString stringWithFormat:@"%i",self.computerPoints];
        //stänger av timer
        [self.timer invalidate];
        self.timer = nil;
        self.startGameButton.hidden= NO;
        
        // om spelet är slut (dator kommer upp i 10 poäng)
        if(self.computerPoints == 10){
            [self.looserSound play];
           self.startGameButton.hidden= YES;
            self.exitGameButton.hidden = NO;
            self.gameOutcomeLabel.hidden = NO;
        }
        
    }
    
}


-(void) viewDidLayoutSubviews{
    [self startLayout];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"pewpew"
                                         ofType:@"wav"]];
    
    self.soundPlayer = [[AVAudioPlayer alloc]
                           initWithContentsOfURL:url
                           error:nil];
    [self.soundPlayer prepareToPlay ];
    
    NSURL *looser = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                            pathForResource:@"loosing"
                                            ofType:@"wav"]];
    self.looserSound = [[AVAudioPlayer alloc]
                        initWithContentsOfURL:looser
                        error:nil];
    [self.looserSound prepareToPlay ];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
