//
//  ViewController.m
//  ThePong
//
//  Created by Anders Zetterström on 2015-02-15.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *pongText;
@property (weak, nonatomic) IBOutlet UIButton *startGameButton;

@end

@implementation ViewController

-(void)viewDidLayoutSubviews{
    self.pongText.center = CGPointMake(self.view.center.x,self.view.center.y/2);
    self.startGameButton.center = CGPointMake(self.view.center.x, self.view.center.y);
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
